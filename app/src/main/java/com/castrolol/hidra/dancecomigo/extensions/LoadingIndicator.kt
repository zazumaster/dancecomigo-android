package com.castrolol.hidra.dancecomigo.extensions

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import com.castrolol.hidra.dancecomigo.extensions.AnkoDatabase
import com.castrolol.hidra.dancecomigo.extensions.AnkoDatabaseFactory
import java.util.*

/**
 * Created by 'Luan on 06/02/2016.
 */
class LoadingIndicator(context: Context){

    val context: Context = context
    var progress: ProgressDialog? = null

    fun show(message:String = "Carregando" ){

        progress = ProgressDialog(context);
        progress!!.setMessage(message!!);
        progress!!.show();

    }

    fun hide(){
        progress!!.hide();
    }

}

object LoadingIndicatorFactory {

    val indicators:MutableMap<Activity, LoadingIndicator> = HashMap()

    fun showFor(activity: Activity, message:String = "Loading"){

        var indicator: LoadingIndicator ;
        if( LoadingIndicatorFactory.indicators.contains(activity)){
            indicator = LoadingIndicatorFactory.indicators.get(activity)!!
        }else{
            indicator = LoadingIndicator(activity)
            LoadingIndicatorFactory.indicators.put(activity, indicator)
        }
        indicator.show(message)
    }

    fun hideFor(activity: Activity){

        var indicator: LoadingIndicator ;
        if( LoadingIndicatorFactory.indicators.contains(activity)){
            indicator =  LoadingIndicatorFactory.indicators.remove(activity)!!
            indicator.hide()
        }
    }

}

fun Activity.showLoading(message:String = "Loading"){
     LoadingIndicatorFactory.showFor(this, message)
}


fun Activity.hideLoading(){
   LoadingIndicatorFactory.hideFor(this)
}
