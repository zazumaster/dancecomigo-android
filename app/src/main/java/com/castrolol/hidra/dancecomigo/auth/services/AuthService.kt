package com.castrolol.hidra.dancecomigo.auth.services

import android.content.Context
import com.castrolol.hidra.dancecomigo.R
import com.castrolol.hidra.dancecomigo.auth.model.Claim
import com.castrolol.hidra.dancecomigo.auth.model.Usuario
import com.castrolol.hidra.dancecomigo.data.repositories.config
import com.castrolol.hidra.dancecomigo.extensions.ContextProvider
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.android.extension.responseJson
import com.google.gson.Gson
import org.json.JSONObject

/**
 * Created by 'Luan on 08/02/2016.
 */

class AuthService(override val context: Context) : ContextProvider {

    private val USUARIO_KEY = "usuario"
    private val ACCES_TOKEN_KEY = "access_token"

    val gson: Gson by lazy { Gson() }


    fun requestLogin(username: String, password: String, callback: (Boolean) -> Unit) {


        Fuel
                .post("/token", listOf("grant_type" to "password", "username" to username, "password" to password))
                .responseString {
                    request, response, result ->

                    val (data, error) = result
                    if (error == null) {

                        var obj = JSONObject(data)

                        config.set(ACCES_TOKEN_KEY, obj.get(ACCES_TOKEN_KEY).toString())

                        callback(true)

                    } else {
                        callback(false)
                    }

                }


    }

    fun register(vararg data: Pair<String, String>, callback: (Boolean, String) -> Unit) {

        var json = JSONObject()
        data.forEach { json.put(it.first, it.second) }

        Fuel
                .post("api/account/register")
                .body(json.toString())
                .header(
                        "Content-Type" to "application/json"
                )
                .response { request, response, result ->

                    val (data, error) = result
                    if (error == null) {
                        callback(true, context.resources.getString(R.string.registered_with_success))
                    } else {
                        callback(false, "error")

                    }
                }

    }

    fun getInfo(callback: (Usuario?) -> Unit) {

        Fuel
                .get("api/account/info")
                .header("authorization" to "bearer " + config.get(ACCES_TOKEN_KEY))
                .responseJson { request, response, result ->

                    var (data, error) = result

                    config.remove(USUARIO_KEY)

                    if (error == null) {
                        var json = data.toString()
                        config.set(USUARIO_KEY, json);

                        var usuario = constructUsuario(json)

                        callback(usuario)
                    } else {
                        callback(null)

                    }

                }

    }

    fun getUsuarioFromCache(): Usuario? {

        var json = config.get(USUARIO_KEY)

        if (json.isNullOrEmpty()) return null
        return constructUsuario(json!!)

    }

    private fun constructUsuario(json: String): Usuario? {
        var data = JSONObject(json)
        var usuario = gson.fromJson(json, Usuario::class.java);

        data!!.keys().forEach {
            usuario.claims!!.add(Claim(it, data.get(it)))
        }
        return usuario
    }

}
