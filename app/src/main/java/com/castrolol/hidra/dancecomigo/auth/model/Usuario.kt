package com.castrolol.hidra.dancecomigo.auth.model

/**
 * Created by 'Luan on 08/02/2016.
 */

data class Usuario(
        val id:String = "",
        val nome:String = "",
        val email:String = "",
        var roles:Array<String> = arrayOf<String>(),
        val claims:MutableList<Claim> = mutableListOf<Claim>()
)