package com.castrolol.hidra.dancecomigo.ui.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

import com.castrolol.hidra.dancecomigo.R
import com.castrolol.hidra.dancecomigo.auth.services.AuthService
import com.castrolol.hidra.dancecomigo.extensions.hideLoading
import com.castrolol.hidra.dancecomigo.extensions.showLoading
import com.castrolol.hidra.dancecomigo.extensions.validate
import org.jetbrains.anko.alert
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick

class RegisterStep1Activity : AppCompatActivity() {

    val authService = AuthService(this)

    var editNome: EditText? = null
    var editEmail: EditText? = null
    var editSenha: EditText? = null
    var editSenhaConfirmar: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_step1)

        editNome = find<EditText>(R.id.et_nome)
        editEmail = find<EditText>(R.id.et_email)
        editSenha = find<EditText>(R.id.et_senha)
        editSenhaConfirmar = find<EditText>(R.id.et_senha_confirmar)

        find<Button>(R.id.bt_done).onClick {

            register()

        }

        var intent = Intent()
        setResult(0, intent)

    }

    private fun register(){

        validate {

            required(editNome)
            message("É necessário que informe seu nome")

            required(editEmail)
            message("É necessário informar seu email");

            regex(editEmail, EMAIL_REGEX)
            message("É importante que seu email sejá válido, verifique-o novamente")

            required(editSenha)
            message("É necessário definir uma senha")

            length(editSenha, min = 6)
            message("Para sua segurança, sua senha deve ter no minimo 6 caracteres")

            required(editSenhaConfirmar)
            message("É necessário inserir novamente sua senha")



            invalid {
                alert(message!!).show()
            }

            valid {
                finishRegister()
            }

        }


    }

    private fun finishRegister() {

        var nome = editNome!!.text.toString()
        var email = editEmail!!.text.toString()
        var senha = editSenha!!.text.toString()
        var senhaConfirmar = editSenhaConfirmar!!.text.toString()

        if (senha != senhaConfirmar) {
            alert("A senhas estão diferentes").show()
            editSenha!!.text.clear()
            editSenhaConfirmar!!.text.clear()
            editSenha!!.requestFocus()
            return
        }

        showLoading("Aguarde, estamos criando seu cadastro!")
        authService.register(
                "nome" to nome,
                "email" to email,
                "senha" to senha
        ) { success, msg ->
            hideLoading()
            if (success) {
                var intent = Intent()
                intent.putExtra("login", email)
                intent.putExtra("senha", senha)
                setResult(1, intent)
                onBackPressed()
            } else {
                alert("Não foi possivel registrar").show()
            }

        }

    }
}
