package com.castrolol.hidra.dancecomigo.data.repositories

import android.content.Context
import com.castrolol.hidra.dancecomigo.extensions.*

/**
 * Created by 'Luan on 06/02/2016.
 */

class ConfigRepository(override val context: Context) : ContextProvider {


    fun get(key: String): String? {
        var value: String? = null;

        context.database.use {

            select("configuration", "value")
                    .where("key = {key}", "key" to key)
                    .exec {
                        if (moveToNext())
                            value = getString(0)


                    }

        }

        return value
    }

    fun set(key: String, value: String) {

        database.use {

            replace("configuration",
                    "key" to key,
                    "value" to value
            )

        }

    }

    fun remove(key: String) {

        database.use {

            delete("configuration",
                    "key = {key}",
                    "key" to key
            )
        }


    }
}

val Context.config: ConfigRepository
    get() = ConfigRepository(applicationContext)


val ContextProvider.config: ConfigRepository
    get() = ConfigRepository(context)