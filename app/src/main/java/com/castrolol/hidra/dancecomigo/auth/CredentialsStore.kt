package com.castrolol.hidra.dancecomigo.auth

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Base64
import com.castrolol.hidra.dancecomigo.auth.model.Claim
import com.castrolol.hidra.dancecomigo.auth.model.Usuario
import com.castrolol.hidra.dancecomigo.auth.services.AuthService
import com.castrolol.hidra.dancecomigo.data.repositories.config
import com.castrolol.hidra.dancecomigo.extensions.ContextProvider
import com.castrolol.hidra.dancecomigo.ui.login.UserLoginActivity
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import org.json.JSONObject
import java.nio.charset.Charset

/**
 * Created by 'Luan on 06/02/2016.
 */
class CredentialsStore(override var context: Context) : ContextProvider {

    val isAuth: Boolean
        get() = !accessToken.isNullOrBlank()

    val accessToken: String?
        get() = config.get("access_token")

    private val usuario: Usuario?
        get() = authService.getUsuarioFromCache()

    private val gson: Gson by lazy { Gson() }

    val authService: AuthService by lazy { AuthService(context) }



    fun require(callback: () -> Unit): Boolean {
        if (accessToken.isNullOrEmpty()) {
            callback();
            return false;
        }
        return true;
    }

    fun resolve(callback: (Usuario?) -> Unit) {

        if (accessToken == null) {
            return
        }

        if (usuario != null) {

            callback(usuario)
        }

        authService.getInfo {
            info ->
            if (info != null)  callback(usuario)
        };

    }

}


val Activity.credentials: CredentialsStore
    get() = CredentialsStore(applicationContext)