package com.castrolol.hidra.dancecomigo.extensions

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.castrolol.hidra.dancecomigo.extensions.ManagedSQLiteAssetOpenHelper

/**
 * Created by 'Luan on 06/02/2016.
 */
val DATABASE_VERSION = 1;
val DATABASE_NAME = "main.db";

class AnkoDatabase(ctx: Context) : ManagedSQLiteAssetOpenHelper(ctx, DATABASE_NAME, null, DATABASE_VERSION) {



}

object AnkoDatabaseFactory {
    private var instance: AnkoDatabase? = null

    fun  getInstance(ctx: Context): AnkoDatabase {

            if (instance == null) {
                instance = AnkoDatabase(ctx.applicationContext)
            }
            return instance!!

    }
}

val Context.database: AnkoDatabase
    get() = AnkoDatabaseFactory.getInstance(applicationContext)

val ContextProvider.database: AnkoDatabase
    get() = AnkoDatabaseFactory.getInstance(context)
