package com.castrolol.hidra.dancecomigo.extensions

import android.app.Activity
import android.support.design.R
import android.widget.EditText

/**
 * Created by 'Luan on 09/02/2016.
 */

class AnkoValidation {

    val EMAIL_REGEX = """\A[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\z"""

    var isValid = true
        private set

    var message: String? = null
        private set

    fun valid(callback: () -> Unit) {
        if (isValid) {
            callback()
        }
    }

    fun invalid(callback: (String?) -> Unit) {
        if (!isValid) {
            callback(message)
        }
    }


    fun message(string: String) {
        if (message.isNullOrEmpty() && !isValid) {
            message = string
        }
    }

    fun required(string: String?) {
        if (isValid == false) return
        if (string.isNullOrEmpty()) isValid = false;
    }

    fun required(field: EditText?) {
        required(field?.text.toString())
    }

    fun regex(string: String?, pattern: String) {
        if (string.isNullOrEmpty()) return
        if( !Regex (pattern).matches(string!!) ){
            isValid = false
        }
    }

    fun regex(field: EditText?, pattern: String) {
        regex(field?.text.toString(), pattern)
    }

    fun length(string: String?, max: Int? = -1, min: Int? = -1) {
        if (string.isNullOrEmpty()) return
        if (max!! > 0 && string!!.length > max!!) isValid = false
        if (min!! > 0 && string!!.length < min!!) isValid = false
    }

    fun length(field: EditText?, max: Int? = -1, min: Int? = -1) {
        length(field?.text.toString(), max, min)
    }
}

fun Activity.validate(validationAction: AnkoValidation.() -> Unit) {
    validationAction(AnkoValidation())
}