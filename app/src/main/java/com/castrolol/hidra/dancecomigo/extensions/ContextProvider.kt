package com.castrolol.hidra.dancecomigo.extensions

import android.content.Context

/**
 * Created by 'Luan on 08/02/2016.
 */
interface ContextProvider {
    val context: Context
}
