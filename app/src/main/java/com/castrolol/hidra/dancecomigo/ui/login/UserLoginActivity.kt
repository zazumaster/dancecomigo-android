package com.castrolol.hidra.dancecomigo.ui.login

import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.castrolol.hidra.dancecomigo.Constants

import com.castrolol.hidra.dancecomigo.R
import com.castrolol.hidra.dancecomigo.auth.services.AuthService
import com.castrolol.hidra.dancecomigo.data.repositories.config
import com.castrolol.hidra.dancecomigo.extensions.hideLoading
import com.castrolol.hidra.dancecomigo.extensions.showLoading
import com.castrolol.hidra.dancecomigo.extensions.validate
import org.jetbrains.anko.alert
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick
import org.json.JSONObject

class UserLoginActivity : AppCompatActivity(), SurfaceHolder.Callback {

    private var mp: MediaPlayer? = null;
    private val authService: AuthService by lazy { AuthService(this) }
    var mSurfaceView: SurfaceView? = null;

    var usernameEdit: EditText? = null
    var passwordEdit: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_login)

        mSurfaceView = find<SurfaceView>(R.id.surface);
        mSurfaceView!!.holder.addCallback(this);

        usernameEdit = find<EditText>(R.id.et_username)
        passwordEdit = find<EditText>(R.id.et_password)


        find<Button>(R.id.bt_entrar).onClick {
            var username = usernameEdit?.text.toString()
            var password = passwordEdit?.text.toString()

            doLogin(username, password)
        }

        find<Button>(R.id.bt_inscrever).onClick {
            doRegister()
        }
    }

    private fun doRegister() {


        startActivityForResult(Intent(this, RegisterStep1Activity::class.java), 0)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {

        if (requestCode == 1 && data != null) {
            if (data.extras.size() == 2) {
                var login = data.extras.getString("login")
                var senha = data.extras.getString("senha")
                doLogin(login, senha)
            }
        }


    }

    fun doLogin(username: String, password: String) {


        validate {

            required(usernameEdit)
            message("É necessário informar o seu email")

            regex(usernameEdit, EMAIL_REGEX)
            message("Verifique seu email, parece ser inválido")

            required(passwordEdit)
            message("É necessário informar sua senha")

            length(password, -1, 6)
            message("Senha inválida!")


            invalid {

                alert(message!!).show()

            }

            valid {

                showLoading("Entrando...");


                authService.requestLogin(username, password) {
                    success ->
                    hideLoading()
                    if (success) {
                        successLogin();
                    } else {
                        failedLogin()
                    }
                }
            }
        }


    }

    override fun onBackPressed(){
        System.exit(0)
    }

    private fun failedLogin() {

        alert("Usuário ou senha invalidos").show()

    }

    private fun successLogin() {

        onBackPressed()
    }

    fun checkIsAuthenticated() {

        var accessToken = config.get("access_token")

        if (accessToken.isNullOrEmpty()) return;

        showLoading("Entrando...")


    }

    private fun save() {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun surfaceChanged(p0: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {
    }

    override fun surfaceCreated(p0: SurfaceHolder?) {
        var video: Uri = Uri.parse("android.resource://" + packageName + "/"
                + R.raw.background_movie);

        mp = MediaPlayer()

        mp!!.setDataSource(this, video);
        mp!!.prepare();


        var display = windowManager.defaultDisplay;
        var childWidthMeasureSpec = View.MeasureSpec.makeMeasureSpec(display.width, View.MeasureSpec.UNSPECIFIED);
        var childHeightMeasureSpec = View.MeasureSpec.makeMeasureSpec(display.height,
                View.MeasureSpec.UNSPECIFIED);
        mSurfaceView!!.measure(childWidthMeasureSpec, childHeightMeasureSpec);

        mp!!.setDisplay(mSurfaceView!!.holder);
        mp!!.isLooping = true;
        mp!!.setVolume(0f, 0f);
        mp!!.setVideoScalingMode(android.media.MediaPlayer
                .VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)
        //Start video
        mp!!.start();
    }

    override fun surfaceDestroyed(p0: SurfaceHolder?) {
    }

}